/*
 * @Author: Turtle (qq769711153@hotmail.com) 
 * @Date: 2021-12-02 10:23:34 
 * @Last Modified by:   Turtle (qq769711153@hotmail.com) 
 * @Last Modified time: 2021-12-02 10:23:34 
 */
#pragma once
#include "BFS.hpp"
#include "DFS.hpp"
#include "GrDijkstra.hpp"
#include "GrMST.hpp"
#include "GrMST2.hpp"