/*
 * @Author: Turtle (qq769711153@hotmail.com) 
 * @Date: 2021-12-02 10:23:30 
 * @Last Modified by:   Turtle (qq769711153@hotmail.com) 
 * @Last Modified time: 2021-12-02 10:23:30 
 */
#pragma once
#include "Graph.hpp"

template <typename PreVisit, typename PostVisit>
void DFS(Graph* G, int v) {
  PreVisit(G, v);
  G->setMark(v, VISITED);
  for (int w = G->first(v); w < G->n(); w = G->next(v, w))
    if (G->getMark(w) == UNVISITED) DFS<PreVisit, PostVisit>(G, w);
  PostVisit(G, v);
}