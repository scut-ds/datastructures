#include <bits/stdc++.h>
using namespace std;
int n; 
int a[1000005] = {0};

int bfs(int x)
{
	queue<int> q;
	q.push(x);
	while(!q.empty())
	{
		int t = q.front();
		q.pop(); // 弹出队头 
		if(t==n)
		{
			return a[t];
		}
		//3种情况 
		if(t-1>1&&t-1<=n&&a[t-1]==0)
		{
			q.push(t-1);
			a[t-1] = a[t] + 1;	
		} 
		if(t+1>1&&t+1<=n&&a[t+1]==0)
		{
			q.push(t+1);
			a[t+1] = a[t] + 1;	
		}
		if(2*t>1&&2*t<=n&&a[2*t]==0)
		{
			q.push(2*t);
			a[2*t] = a[t] + 1;	
		}
	}
}

int main()
{
	cin>>n;
	cout<<bfs(1);
    return 0;
}
 
