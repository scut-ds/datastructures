﻿#include<iostream>
#include<algorithm>
int n;
int count = 0;
int ans[15] = { 0 };
int line1[15] = { 0 };//列
int line2[30] = { 0 };//顺对角线
int line3[30] = { 0 };//逆对角线
void setvalue(int step, int i, int k)
{
    ans[step] = i;
    line1[i] = k;
    line2[step + i] = k;
    line3[i - step + n] = k;
}

void dfs(int step)
{
    //结束条件
    if (step > n)
    {
        count++;//说明找到前面n个皇后
        if (count <= 3)
        {
            for (int i = 1; i <= n; i++)
            {
                std::cout << ans[i] << " ";
            }
            std::cout << std::endl;
        }
        return;
    }
    //递归
    for (int i = 1; i <= n; i++)
    {
        if (line1[i] | line2[i + step] | line3[i - step + n]) continue;
        setvalue(step, i, 1);
        dfs(step + 1);
        setvalue(step, i, 0);
    }
}

int main()
{
    std::cin >> n;
    dfs(1);//从第一行开始
    std::cout << count;
    return 0;
}